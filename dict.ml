(**
 * Dictionary module that stores key-value pairs 
 * 
 * @author Max Jonsson
 * @version 2019-11-28
*)

module Dict : sig

  type ('a, 'b) dict = Dict of ('a * 'b) list

  val make: ('a * 'b) list -> ('a, 'b) dict
  val empty: ('a, 'b) dict

  val add: ('a * 'b) -> ('a, 'b) dict -> ('a, 'b) dict
  val remove:  'a -> ('a, 'b) dict -> ('a, 'b) dict
  val exists: 'a -> ('a, 'b) dict -> bool

  (* val length: ('a, 'b) dict -> int *)
  (* val append: ('a, 'b) dict -> ('a, 'b) dict -> ('a, 'b) dict *)
  (* val replace: 'a -> 'b -> ('a, 'b) dict -> ('a, 'b) dict *)

  (* val keys: ('a, 'b) dict -> 'a list *)
  (* val values: ('a, 'b) dict -> 'b list *)

  val map: ('b -> 'c) -> ('a, 'b) dict -> ('a, 'c) dict 
  val inverse: ('a, 'b) dict -> ('b, 'a) dict

  (* val zip: 'a list -> 'b list -> ('a, 'b) dict *)
  (* val unzip: ('a, 'b) dict -> ('a list * 'b list) *)
  val enumerate: 'a list -> (int, 'a) dict

  val to_string: ('a, 'b) dict -> ('a -> string) -> ('b -> string) -> string

end = struct

  type ('a, 'b) dict = Dict of ('a * 'b) list

  let make (l: ('a * 'b) list): ('a, 'b) dict = Dict l
  let empty: ('a, 'b) dict = make []

  let add ((k: 'a), (v: 'b)) (d: ('a, 'b) dict): ('a, 'b) dict =
    match d with
    | Dict l -> (k, v) :: l |> make

  let rec remove (k: 'a) (d: ('a, 'b) dict): ('a, 'b) dict =
    match d with
    | Dict [] -> empty
    | Dict ((key, _)::t) when key = k -> make t |> remove k
    | Dict (h::t) -> make t |> remove k |> add h

  let rec exists (k: 'a) (d: ('a, 'b) dict): bool =
    match d with
    | Dict [] -> false
    | Dict ((key, _)::t) when key = k -> true
    | Dict (_::t) -> make t |> exists k

  let rec map (f: ('b -> 'c)) (d: ('a, 'b) dict): ('a, 'c) dict =
    match d with
    | Dict [] -> empty
    | Dict ((k, v)::t) -> make t |> map f |> add (k, (f v))

  let rec inverse (d: ('a, 'b) dict): ('b, 'a) dict =
    match d with
    | Dict [] -> empty
    | Dict ((k, v)::t) -> make t |> inverse |> add (v, k)

  let enumerate (lst: 'a list): (int, 'a) dict =
    let rec aux (l: 'a list) (n: int): (int, 'a) dict = 
      match lst with
      | [] -> empty
      | h::t -> n + 1 |> aux t |> add (n, h)
    in aux lst 0

  let rec to_string (d: ('a, 'b) dict) (f1: ('a -> string)) (f2: ('b -> string)): string =
    let stringify ((k: 'a), (v: 'b)) (f1: ('a -> string)) (f2: ('b -> string)): string =
      Printf.sprintf "(%s, %s)" (f1 k) (f2 v) in
    let rec aux (d: ('a, 'b) dict) (f1: ('a -> string)) (f2: ('b -> string)): string list =
      match d with
      | Dict [] -> []
      | Dict (h::t) -> stringify h f1 f2 :: aux (make t) f1 f2
    in aux d f1 f2 |> Batteries.join "\n"
end

include Dict


let () = 
  let d = Dict.empty in
  let d = Dict.add ("hej", 1) d in
  let d = Dict.add ("hallo", 2) d in
  let d = Dict.remove "hej" d in
  let d = Dict.inverse d in
  let d = Dict.inverse d in
  let d = Dict.map (fun x -> x + 2) d in
  let b1 = Dict.exists "hallo" d in
  let b2 = Dict.exists "hej" d in 
  Printf.printf "True: %s, False: %s" (string_of_bool b1) (string_of_bool b2)
(*Printf.printf "%s" (to_string d (fun s -> s) string_of_int)*)