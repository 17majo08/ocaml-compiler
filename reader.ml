(**
 * This module reads a tokenized compiler expression
 * and transforms it into an Abstract Syntax Tree (AST).
 *
 * @author Max Jonsson
 * @version 2019-11-26
*)

module Ast : sig

  type ast =
    | Imm of int  (* immediate value *)
    | Arg of int  (* reference to n-th argument *)
    | Add of (ast * ast) (* add first to second *)
    | Sub of (ast * ast) (* subtract second from first *)
    | Mul of (ast * ast) (* multiply first by second *)
    | Div of (ast * ast) (* divide first by second *)

  val to_string: ast -> string

end = struct

  type ast =
    | Imm of int
    | Arg of int
    | Add of (ast * ast)
    | Sub of (ast * ast)
    | Mul of (ast * ast)
    | Div of (ast * ast)

  let rec to_string (a: ast): string =
    match a with
    | Imm i -> Printf.sprintf "%d" i
    | Arg i -> Printf.sprintf "Arg%d" i
    | Add (a, b) -> Printf.sprintf "(%s + %s)" (to_string a) (to_string b)
    | Sub (a, b) -> Printf.sprintf "(%s - %s)" (to_string a) (to_string b)
    | Mul (a, b) -> Printf.sprintf "(%s * %s)" (to_string a) (to_string b)
    | Div (a, b) -> Printf.sprintf "(%s / %s)" (to_string a) (to_string b)

end

let read ((args: string list), (expr: string list)): Ast.ast =
  let dict = args |> List.filter (fun a -> (a = "[") <> (a = "]")) |>
             Dict.enumerate |> Dict.inverse |> Dict.map (fun i -> Ast.Arg i) in
  Imm 0