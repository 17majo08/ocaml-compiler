module Batteries : sig

  (* String functions *)
  val tokenize : string -> string list
  val join : string -> string list -> string
  val join2 : string -> 'a list -> ('a -> string) -> string

  (* List functions *)
  val print_list : 'a list -> ('a -> string) -> unit

end = struct

  let tokenize (str: string): string list =
    let rec aux (s: string) (acc: string list) =
      let len = String.length s in
      match len < 1 with
      | true -> acc
      | false -> String.sub s 0 1 :: ((len-1 |> String.sub s 1 |> aux) acc)
    in aux str []

  let join (sep: string) (lst: string list) =
    let rec aux lst sep acc =
      match lst with
      | [] -> acc
      | h::[] -> acc ^ sep ^ h
      | h::t -> acc ^ sep ^ h |> aux t sep
    in
    match lst with
    | [] -> ""
    | h::t -> aux (List.tl lst) sep (List.hd lst)

  let join2 (sep: string) (lst: 'a list) (f: ('a -> string)) =
    List.map f lst |> join sep

  let print_list (l: 'a list) (f: ('a -> string)): unit =
    let rec aux (l: 'a list) (f: ('a -> string)) (acc: string): string =
      match l with
      | [] -> acc
      | h::t -> acc ^ (f h) |> aux t f
    in aux l f "" |> Printf.printf "%s"

end


include Batteries
