(**
 * This module is used to tokenize a compiler expression.
 * 
 * A compiler expression is sent in as a string on the form:
 *
 *      " [ x y ] (x+y) "   ->   fun x y -> x + y
 * 
 * and the goal of this module is to tokenize the expression
 * into a list of single tokens:
 *
 *      ( [ "["; "x"; "y"; "]" ], [ "("; "x"; "+"; "y"; ")" ] ) 
 * 
 * @author Max Jonsson
 * @version 2019-11-26
*)

module Tokenizer: sig

  val tokenize: string -> (string list * string list)

end = struct

  let split_arg_exp (str: string): (string * string) =
    let arg_start = String.index str '[' in
    let arg_end = 1 + String.index str ']' in
    let args = String.sub str arg_start (arg_end - arg_start) |> String.trim in
    let expr = String.sub str arg_end (String.length str - arg_end) |> String.trim in
    (args, expr)

  (* Private *)
  let is_arg (x: string) (args: string list): bool = 
    List.exists (fun elt -> elt = x) args

  (* Private *)
  let add_spacing (l: string list) (args: string list): string list =
    List.map
      (fun a -> if is_arg a args then " " ^ a ^ " " else a) l

  (* Private *)
  let pre_process (expr: string) (args: string list): string =
    List.fold_right (fun elt acc -> elt ^ acc) (
      add_spacing (Batteries.tokenize expr) args
    ) ""

  let to_list ((args, expr): (string * string)): (string list * string list) =
    let arg_tokens = String.split_on_char ' ' args in
    let expr_tokens =
      pre_process expr arg_tokens |> String.split_on_char ' ' in
    (arg_tokens, expr_tokens)

  let tokenize (expr: string): (string list * string list) =
    expr |> split_arg_exp |> to_list

end

include Tokenizer