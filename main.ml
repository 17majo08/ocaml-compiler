let app (l1, l2) = l1 @ l2

let () = 
  "[ x y ] (x+y) / 2" |> Tokenizer.tokenize |> app |> (* Reader.read |> *)
  Batteries.join " , " |> Printf.printf "%s" 